const { GraphQLServer } = require('graphql-yoga')

/** Demo user data */
const users = [
	{
		id: 1,
		name: 'Darío',
		email: 'dario@example.com',
		age: 28,
	},
	{
		id: 2,
		name: 'Andrew',
		email: 'andrew@test.com',
		age: 27,
	},
	{
		id: 3,
		name: 'Sarah',
		email: 'sarah@example.com',
	},
]

const posts = [
	{
		id: 1,
		title: 'A new post',
		body: 'An awesome content',
		published: true,
		author: 1,
	},
	{
		id: 2,
		title: 'Another post',
		body: 'Javascript is awesome',
		published: false,
		author: 3,
	},
	{
		id: 3,
		title: 'PHP',
		body: 'PHP is really lame',
		published: true,
		author: 2,
	},
	{
		id: 4,
		title: 'Test post',
		body: 'This post is pointless',
		published: false,
		author: 5,
	},
]

/**
 * Type definitions
 * exclamation mark forces the type on the query. If absent we can return null
 * Types must begin with an upper case letter.
 * When we define a custom type, we need to add it to
 * the query type and assign it into a value, when we query a non
 * scalar value we need to specify on the query which fields we want
 * to retrieve
 */
const typeDefs = `
  type Query {
		users(query: String): [User!]!
		posts(query: String): [Post!]!
    greeting(name: String, age: Int): String!
    add (a: Float!, b: Float!): Float!
    addArray(numbers: [Float!]!): Float!
    grades: [Int!]!
    id: ID!
    hello: String!
    name: String!
    location: String!
    bio: String!
    age: Int!
    employed: Boolean!
    gpa: Float
    title: String!
    price: Float!
    releaseYear: Int
    rating: Float
    inStock: Boolean
    me: User!
    post: Post!
  }

  # comments inside the typedef
  type User {
    id: ID!
    name: String!
    email: String!
    age: Int
  }

  type Post {
    id: ID!
    title: String!
    body: String!
		published: Boolean!
		author: User!
  }
`

// Resolvers
const resolvers = {
	Query: {
		/**
		 * These functions can receive up to 4 arguments.
		 * Parent: In relational data parent is used frequently to refer the upper
		 * type that contains that query.
		 * Args: Contain the arguments we need to pass to the function through the query
		 * Ctx: Context is useful for contextual data, if the user is logged in, etc.
		 * Info: Contains information about the metadata of the request.
		 */
		posts: (parent, args, ctx, info) =>
			args.query
				? posts.filter(post => post.title.includes(args.query) || post.body.includes(args.query))
				: [...posts],
		users: (parent, args, ctx, info) =>
			!args.query ? [...users] : users.filter(user => user.name.includes(args.query)),
		greeting: (parent, args, ctx, info) => {
			const { name, age } = args
			return name ? `Hello ${name} ${age}` : `Hello anon ${age}`
		},
		add: (parent, args) => args.a + args.b,
		addArray: (parent, args) => args.numbers.reduce((a, b) => a + b, 0),
		grades: () => [90, 80, 66],
		id: () => 'abc123',
		hello: () => 'This is my first query',
		name: () => 'Darío Navarrete',
		location: () => 'Mexico City',
		bio: () => `I'm a Javascript programmer.`,
		age: () => 28,
		employed: () => true,
		gpa: () => 3.4,
		title: () => `Sorcerers stone`,
		price: () => 12.99,
		releaseYear: () => 2009,
		rating: () => 4.78,
		inStock: () => false,
		me: () => ({
			id: '1234abcd',
			name: 'Darío',
			email: 'test@test.com',
		}),
		post: () => ({
			id: 'post1',
			title: 'One regular post',
			body: 'Blogs again?',
			published: true,
		}),
	},
	Post: {
		author: (parent, args, ctx, info) => {
			// parent is the reference of the object that called this function, a kind
			// of 'this'
			return users.find(user => parent.author === user.id)
		},
	},
}

const props = { typeDefs, resolvers }
const server = new GraphQLServer(props)

server.start(() => console.log('Server running'))
